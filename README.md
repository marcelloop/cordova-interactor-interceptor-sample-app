# cordova-interactor-interceptor-sample-app

###setup interactor event
####event
setup an event with the following data within the [interactor backend](https://interactor.swisscom.ch)

* name: normalEvent
* description: this is a normal event
* assign a zone
* type : custom

put the following json in the custom field
```
{
	"en": {
		"name": "Zone Enter",
		"description": "The user entered the zone"
	},
	"de": {
		"name": "Zone betreten",
		"description": "Der Benutzer betrat die Zone"
	}
}
```
Setup another event

* name: filteredEvent
* description: the notification from this event should be filtered
* assign a zone
* type : custom

put the following json in the custom field
```
{
	"en": {
		"name": "Zone Enter filtered event",
		"description": "The user entered the zone"
	},
	"de": {
		"name": "Zone betreten filtered event",
		"description": "Der Benutzer betrat die Zone"
	}
}
```
###setup application for android and ios
####clone repo

```
git clone https://bitbucket.org/marcelloop/cordova-interactor-interceptor-sample-app.git
cd cordova-interactor-interceptor-sample-app
```

####install cordova
```
npm install -g cordova
```

####add platform
```
cordova platform add android
cordova platform add ios
```

####add cordova plugins
```
cordova plugin add cordova-plugin-whitelist
cordova plugin add cordova-plugin-app-event
cordova plugin add https://bitbucket.org/interactdev/cordova-interactor-plugin-public.git
```

####add your api key
in ```<project-root>/www/js/index.js``` add your api key or [other configuration](https://interactor-swisscom.readme.io/docs/getting-started-with-cordova)

```
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        var configOptions = {
            apiKey: "<your-api-key>",
            server: "https://interactor.swisscom.ch"
        };
        var configNotification = {
            receiveDuring: Interactor.NotificationStates.BOTH, //optional
            logging: true,
            checkBluetoothAccess: true,                //optional, iOS only
            checkLocationAccess: true,                 //optional, iOS only
            backgroundScanInterval: 10000,             //optional, Android only
            foregroundScanInterval: 5000,               //optional, Android only
            stackText: false                          //optional, Android only

        };
},
```

####build on android
#####download filter for android
Watch out that you call this commands in your ```<project root>```
```
wget -P platforms/android/src/io/cordova/hellocordova https://bitbucket.org/marcelloop/cordova-interactor-interceptor-sample-app/downloads/MyNotificationFilter.java
```
```
cordova build android
cordova run android
```

####build on ios
#####download filter for ios
Watch out that you call this commands in your ```<project root>```
```
wget -P platforms/ios/HelloCordova/Classes https://bitbucket.org/marcelloop/cordova-interactor-interceptor-sample-app/downloads/ExtendedNotificationModifier.h
wget -P platforms/ios/HelloCordova/Classes https://bitbucket.org/marcelloop/cordova-interactor-interceptor-sample-app/downloads/ExtendedNotificationFilter.h
wget -P platforms/ios/HelloCordova/Classes https://bitbucket.org/marcelloop/cordova-interactor-interceptor-sample-app/downloads/ExtendedNotificationModifier.m
wget -P platforms/ios/HelloCordova/Classes https://bitbucket.org/marcelloop/cordova-interactor-interceptor-sample-app/downloads/ExtendedNotificationFilter.m
```

```
cordova build ios
```
* now you have to open ```<project-root>/platforms/ios/HelloCordova.xcodeproj``` with Xcode 
+ Right click on "Classes" --> "Add Files to "HelloCordova" --> select these 4 files 
	* ExtendedNotificationModifier.h
	* ExtendedNotificationFilter.h
	* ExtendedNotificationModifier.m	
	* ExtendedNotificationFilter.m	
* Click add
* Build Project in Xcode



##FAQ
###filter
####Q:      
How do I add an Event to the blacklist?                            
####A:        
if you want to filter out an event, just add the event name into the blacklist. You find the blacklist in ```<project-root>/www/js/index.js```. Open the file and add the event name into the ```blacklist``` Array.
```
var userData = {"language" : "de", "blacklist" : ["filteredEvent","ZoneEnter99"]};
```              


####modify notification
####Q:
How can I change the language of the notification
####A:
Modify in ```<project-root>/www/js/index.js```  the ```userData``` part.

If you want to get the notification in english put "en" into the ```language``` property. **Its important that you use the same property as you have specified in your custom json within your event**.
```
var userData = {"language" : "en", "blacklist" : ["filteredEvent","ZoneEnter99"]};
```

###Please Note
After you made some changes you have to call again
```
cordova build yourPlatform
cordova run yourPlatform
```
Please visit the [interactor readme.io](https://interactor-swisscom.readme.io/docs/getting-started-with-cordova) for further information regarding how to setup a notification modifier and filter